import { CredentialDisplayAttribute, CredentialDisplayData, CredentialViewItem, Display } from '../types/types';

function createMetadataEntry(value: any, i18n: string): CredentialDisplayAttribute {
  return { value, disclosed: true, i18n };
}

function setLabelBasedOnLocale(display: Display[], lang: string): string {
  const language = lang !== 'en' ? `${lang}-${lang.toUpperCase()}` : 'en-US';
  const languageItem = display.find((item: any) => item.locale === language);
  return languageItem?.name || '';
}

export function composeCredentialDisplayData(item: CredentialViewItem, language: string = 'en') {
  const credentialData: CredentialDisplayData = {
    cardDisplay: {
      title: '',
      logoURL: '',
      textColor: '',
      backgroundImage: '',
      backgroundColor: '',
      verified: true,
      cloud: false,
    },
    renderHint: [],
    attributes: {},
    metadata: {},
  };

  const { credential, well_known } = item;
  const { presentation } = credential;
  const { credentialSubject, iss, exp, nbf, iat } = presentation;

  if (!credentialSubject) {
    throw new Error('presentation is missing mandatory key credentialSubject');
  }

  let requestedAttributes: string[] | undefined;

  function requested(key: string) {
    return requestedAttributes ? requestedAttributes.includes(key) : true;
  }

  credentialData.attributes = Object.fromEntries(
    Object.entries(credentialSubject).map(([key, value]) => [
      key,
      { label: key, value: value as string, disclosed: requested(key) },
    ])
  );

  if (well_known?.display?.length > 0) {
    for (const key of Object.keys(credentialSubject)) {
      const entry = item.well_known.credentialSubject[key];

      if (entry) {
        credentialData.attributes[key].label = setLabelBasedOnLocale(entry.display, language);
      }
    }

    const display = well_known.display[0];
    credentialData.cardDisplay = {
      backgroundColor: display.background_color || '',
      backgroundImage: display.background_image?.url || '',
      logoURL: display.logo?.url || '',
      textColor: display.text_color || '',
      title: display.name,
      verified: true,
    };
  } else {
    credentialData.cardDisplay.title = item.credential.presentation.type;
  }

  if (well_known?.render_hint) {
    credentialData.renderHint = well_known.render_hint;
  }

  credentialData.metadata['iss'] = createMetadataEntry(
    `${iss.substring(0, 16)}...${iss.substring(192, 195)}`,
    'wallet:issuer'
  );
  credentialData.metadata['iat'] = createMetadataEntry(new Date(iat * 1000).toLocaleString(), 'wallet:issue_date');
  credentialData.metadata['nbf'] = createMetadataEntry(new Date(nbf * 1000).toLocaleString(), 'wallet:valid_from');
  credentialData.metadata['exp'] = createMetadataEntry(new Date(exp * 1000).toLocaleString(), 'wallet:expiry_date');
  credentialData.metadata['type'] = createMetadataEntry(presentation.type, 'wallet:type');

  return credentialData as CredentialDisplayData;
}

export function mapCredentialView(credentialItems: CredentialViewItem[]) {
  const mapCredentialsToView = (credentials: CredentialViewItem[]) => {
    return new Map(credentials.map(credentialItem => [credentialItem.credential.key_id, credentialItem]));
  };

  return mapCredentialsToView(credentialItems);
}
