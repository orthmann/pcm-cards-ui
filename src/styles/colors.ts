const tintColorLight = 'rgb(226, 0, 116)';

export default {
  primary: '#E20074',
  primaryPressed: '#c00063',
  success: '#22c55e',
  light: {
    text: '#0a0a0a',
    background: '#fbfbfb',
    tint: '#E2E2E2',
    darkTint: '#BDBDBD',
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
  },
};
