import { Platform, StyleSheet } from 'react-native';

import colors from './colors';

export const fontFamily = StyleSheet.create({
  teleNeoOffice: {
    ...Platform.select({
      ios: {
        fontFamily: 'TeleNeoOffice',
      },
      android: {
        fontFamily: 'TeleNeoOffice',
      },
      default: {
        backgroundColor: 'TeleNeoOffice',
      },
    }),
  },
});

export const modeIndependentStyles = StyleSheet.create({
  // credential card view
  cardStatusBar: {
    display: 'flex',
    flexDirection: 'row',
    position: 'absolute',
    width: '100%',
    paddingTop: 12,
    paddingEnd: 12,
    marginTop: -18,
    justifyContent: 'flex-end',
  },
  cardStatusIcon: {
    height: 20,
    elevation: 3,
    width: 20,
    marginLeft: 4,
  },
  card: {
    width: '100%',
    borderRadius: 12,
    borderWidth: 1,
    overflow: 'hidden',
  },
  cardStacked: {
    overflow: 'hidden',
    marginBottom: -30,
    height: 70,
  },
  // credential detail view
  credentialDetailsWrapper: {
    paddingBottom: 24,
    marginTop: 24,
    gap: 12,
    marginHorizontal: '4%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  // credential additional attributes
  attributeBox: {
    display: 'flex',
    backgroundColor: 'white',
    borderRadius: 12,
    elevation: 4,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    marginTop: 4,
    overflow: 'hidden',
  },
  additionalAttribute: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
  disclosed: {
    backgroundColor: 'white',
  },
  undisclosedAdditional: {
    backgroundColor: 'grey',
    opacity: 0.4,
  },
  // general text
  text: {
    fontSize: 16,
    ...fontFamily.teleNeoOffice,
  },
  bold: {
    fontWeight: '700',
  },
  title: {
    fontSize: 20,
    fontWeight: '700',
    ...fontFamily.teleNeoOffice,
  },
  keyText: {
    fontSize: 17,
    fontWeight: '700',
    ...fontFamily.teleNeoOffice,
  },
  helpTextColor: {
    color: 'gray',
  },
  bottomButtonWrapper: {
    height: '10%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'nowrap',
  },
  // buttons
  button: {
    alignSelf: 'center',
    width: '100%',
    paddingHorizontal: 16,
    paddingVertical: 12,
    borderRadius: 8,
    fontWeight: '700',
    textAlign: 'center',
    ...fontFamily.teleNeoOffice,
    overflow: 'hidden',
  },
  buttonPrimary: {
    color: 'white',
    backgroundColor: colors.primary,
  },
  buttonPrimaryInactive: {
    color: 'white',
    backgroundColor: 'lightgray',
  },
  buttonSecondary: {
    backgroundColor: colors.light.background,
    color: colors.light.text,
    borderColor: colors.light.text,
    borderWidth: 1,
  },
});

export const lightStyles = StyleSheet.create({
  background: {
    backgroundColor: colors.light.background,
  },
  lightBackground: {
    backgroundColor: colors.light.tint,
  },
  textColor: {
    color: colors.light.text,
    textDecorationColor: colors.light.text,
  },
  notSelectedColor: {
    borderColor: colors.light.text,
  },
  borderBottomColor: {
    borderBottomColor: colors.light.text,
  },
  thickBorderBottomColor: {
    borderBottomColor: colors.light.tint,
  },
  tint: {
    backgroundColor: colors.light.tint,
  },
  tintDark: {
    backgroundColor: colors.light.darkTint,
  },
  cardBorderColor: {
    borderColor: colors.light.text,
  },
  menuTextColor: {
    color: 'black',
  },
  textInputColor: {
    borderColor: 'gray',
  },
});
