import { lightStyles, modeIndependentStyles } from './styles';

export function useStyles() {
  const colorScheme = lightStyles;

  const styles = {
    ...colorScheme,
    ...modeIndependentStyles,
  };

  return styles;
}
