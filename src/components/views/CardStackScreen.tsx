import { useState } from 'react';
import { CredentialView } from '../../types/types';
import DetailsDisplay from '../card/DetailsDisplay';
import CardStack from './CardStack';

interface CardStackScreenProps {
  credentialView: CredentialView;
  openAll: boolean;
}

const CardStackScreen = ({ credentialView, openAll }: CardStackScreenProps) => {
  const [showDetails, setShowDetails] = useState(false);
  const [selectedKeyID, setSelectedKeyID] = useState('');

  return (
    <>
      <CardStack
        credentialView={credentialView}
        setSelectedKeyID={setSelectedKeyID}
        setShowDetails={setShowDetails}
        openAll={openAll}
      />
      {selectedKeyID !== '' && (
        <DetailsDisplay
          credentialView={credentialView}
          showDetails={showDetails}
          setShowDetails={setShowDetails}
          selectedKeyID={selectedKeyID}
        />
      )}
    </>
  );
};

export default CardStackScreen;
