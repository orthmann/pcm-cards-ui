import { SafeAreaView } from 'react-native';
import { useStyles } from '../../styles/useStyles';
import { mapCredentialView } from '../../utils/credentials';
import CardStackScreen from './CardStackScreen';
import { useEffect, useState } from 'react';
import { CredentialViewItem } from '../../types/types';

const Main = () => {
  const styles = useStyles();
  const [credentialView, setCredentialView] = useState(new Map<string, CredentialViewItem>());

  useEffect(() => {
    const getCredentialView = async () => {
      try {
        const data = await fetch('http://localhost:8081/data.json');
        const credentialView = await data.json();

        setCredentialView(mapCredentialView(credentialView));
      } catch (error) {
        console.error(error);
      }
    };

    getCredentialView();
  }, []);

  return (
    <SafeAreaView style={[styles.background, { flex: 1, paddingTop: 30 }]}>
      <CardStackScreen
        credentialView={credentialView}
        openAll
      />
    </SafeAreaView>
  );
};

export default Main;
