import { useState } from 'react';
import { FlatList, Pressable, View } from 'react-native';
import { useStyles } from '../../styles/useStyles';
import { CredentialView, CredentialViewItem } from '../../types/types';
import { composeCredentialDisplayData } from '../../utils/credentials';
import Card from '../card/Card';

interface CardStackProps {
  credentialView: CredentialView;
  setSelectedKeyID: React.Dispatch<React.SetStateAction<string>>;
  setShowDetails: React.Dispatch<React.SetStateAction<boolean>>;
  openAll: boolean;
}

const CardStack = ({ credentialView, setSelectedKeyID, setShowDetails, openAll }: CardStackProps) => {
  const styles = useStyles();
  const [openedCardKey, setOpenedCardKey] = useState('');

  const handleCardPress = (key: string, stacked: boolean) => {
    setSelectedKeyID(key);
    if (stacked) {
      setOpenedCardKey(key);
    } else {
      setShowDetails(true);
    }
  };

  const renderItem = ({ item, index }: { item: [string, CredentialViewItem]; index: number }) => {
    const [key, credentialViewItem] = item;
    const credentialDisplayData = composeCredentialDisplayData(credentialViewItem);
    const stacked = !openAll && index < credentialView.size - 1 && openedCardKey !== key;

    return (
      <Pressable
        onPress={() => handleCardPress(key, stacked)}
        style={{ marginVertical: 8 }}
      >
        <Card
          credentialData={credentialDisplayData}
          stacked={stacked}
        />
      </Pressable>
    );
  };

  return (
    <View style={[{ height: '100%' }, styles.background]}>
      <View style={{ marginHorizontal: 8 }}>
        <FlatList
          data={Array.from(credentialView)}
          keyExtractor={item => item[0]}
          renderItem={renderItem}
        />
      </View>
    </View>
  );
};

export default CardStack;
