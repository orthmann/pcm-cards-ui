import { useTranslation } from 'react-i18next';
import { Text, View } from 'react-native';
import { useStyles } from '../../styles/useStyles';
import { CredentialDisplayAttribute } from '../../types/types';

interface CredentialAttributeProps {
  attributeData: CredentialDisplayAttribute;
}

const AdditionalAttribute = ({ attributeData }: CredentialAttributeProps) => {
  const styles = useStyles();
  const { t } = useTranslation();

  return (
    <View
      style={[styles.additionalAttribute, attributeData.disclosed ? styles.disclosed : styles.undisclosedAdditional]}
    >
      <Text style={[styles.keyText, styles.textColor]}>
        {attributeData.label || (attributeData.i18n ? t(attributeData.i18n) : '')}:
      </Text>
      <Text style={[styles.text, styles.textColor]}>
        {(attributeData.value ?? '').length > 32
          ? `${(attributeData.value ?? '').slice(0, 12)}...${(attributeData.value ?? '').slice(-12)}`
          : attributeData.value ?? ''}
      </Text>
    </View>
  );
};

export default AdditionalAttribute;
