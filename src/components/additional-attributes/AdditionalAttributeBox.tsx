import { Text, View } from 'react-native';
import { useStyles } from '../../styles/useStyles';
import { CredentialDisplayAttributeList } from '../../types/types';
import AdditionalAttribute from './AdditionalAttribute';

interface AdditionalAttributeBoxProps {
  boxLabel?: string;
  attributeList: CredentialDisplayAttributeList;
}

const AdditionalAttributeBox = ({ boxLabel, attributeList }: AdditionalAttributeBoxProps) => {
  const styles = useStyles();

  return (
    <View>
      <Text style={[styles.text, styles.helpTextColor]}>{boxLabel}</Text>
      <View style={styles.attributeBox}>
        {Object.keys(attributeList).map((key: string) => {
          return (
            <AdditionalAttribute
              key={key}
              attributeData={attributeList[key]}
            />
          );
        })}
      </View>
    </View>
  );
};

export default AdditionalAttributeBox;
