import { Image, View } from 'react-native';
import { useStyles } from '../../styles/useStyles';
import { CardDisplay } from '../../types/types';

interface CardStatusProps {
  status: CardDisplay;
}

const CardStatus = ({ status }: CardStatusProps) => {
  const styles = useStyles();

  return (
    <View style={styles.cardStatusBar}>
      {status?.cloud !== undefined && status.cloud && (
        <View style={styles.cardStatusIcon}>
          <Image source={{ uri: '/assets/icons/cloud-icon.svg' }} />
        </View>
      )}
      {status?.cloud !== undefined && !status.cloud && (
        <View style={styles.cardStatusIcon}>
          <Image source={{ uri: '/assets/icons/phone-icon.svg' }} />
        </View>
      )}
      {status?.verified !== undefined && status.verified && (
        <View style={styles.cardStatusIcon}>
          <Image source={{ uri: '/assets/icons/verify-icon.svg' }} />
        </View>
      )}
    </View>
  );
};

export default CardStatus;
