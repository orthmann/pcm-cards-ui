import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Image, Modal, Pressable, SafeAreaView, ScrollView, Text, View } from 'react-native';
import colors from '../../styles/colors';
import { useStyles } from '../../styles/useStyles';
import { CredentialView } from '../../types/types';
import { composeCredentialDisplayData } from '../../utils/credentials';
import Button from '../button/Button';
import CardWithDetails from './CardWithDetails';

interface DetailsDisplayProps {
  credentialView: CredentialView;
  showDetails: boolean;
  setShowDetails: any;
  selectedKeyID: string;
}

const DetailsDisplay = ({ credentialView, showDetails, setShowDetails, selectedKeyID }: DetailsDisplayProps) => {
  const styles = useStyles();
  const { t } = useTranslation();
  const [deleteConfirmed, setDeleteConfirmed] = useState(false);

  async function deleteCredential() {
    if (deleteConfirmed) {
      // TODO: delete credential

      setShowDetails(false);
      setDeleteConfirmed(false);
    } else {
      setDeleteConfirmed(true);
    }
  }

  const selectedCredential = credentialView.get(selectedKeyID);

  if (selectedCredential) {
    const credentialDisplayData = composeCredentialDisplayData(selectedCredential);
    return (
      <Modal
        presentationStyle="pageSheet"
        animationType="slide"
        transparent={false}
        visible={showDetails}
        onRequestClose={() => {
          setShowDetails(false);
          setDeleteConfirmed(false);
        }}
      >
        <SafeAreaView style={{ height: '100%' }}>
          <View
            style={{
              backgroundColor: colors.primary,
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: 12,
              paddingVertical: 12,
            }}
          >
            <Text style={[styles.title, { color: 'white' }]}>{t('navigation:details')}</Text>
            <Pressable
              onPress={() => {
                setShowDetails(false);
              }}
            >
              <Image source={{ uri: '/assets/icons/close-icon.svg' }} />
            </Pressable>
          </View>
          <ScrollView>
            <CardWithDetails credentialDisplayData={credentialDisplayData} />
          </ScrollView>
          <View style={styles.bottomButtonWrapper}>
            <Button
              text={deleteConfirmed ? t('wallet:delete_credential_confirmation') : t('wallet:delete_credential')}
              variant={deleteConfirmed ? 'primary' : 'secondary'}
              active
              callback={() => deleteCredential()}
            />
          </View>
        </SafeAreaView>
      </Modal>
    );
  } else {
    return <></>;
  }
};

export default DetailsDisplay;
