import { useTranslation } from 'react-i18next';
import { View } from 'react-native';
import { useStyles } from '../../styles/useStyles';
import { CredentialDisplayData } from '../../types/types';
import AdditionalAttributeBox from '../additional-attributes/AdditionalAttributeBox';
import Card from './Card';

interface CardWithDetailsProps {
  credentialDisplayData: CredentialDisplayData;
}

const CardWithDetails = ({ credentialDisplayData }: CardWithDetailsProps) => {
  const styles = useStyles();
  const { t } = useTranslation();

  return (
    <View style={styles.credentialDetailsWrapper}>
      <Card credentialData={credentialDisplayData} />
      <AdditionalAttributeBox
        boxLabel={t('wallet:credential_details')}
        attributeList={credentialDisplayData.attributes}
      />
      <AdditionalAttributeBox
        boxLabel={t('wallet:metadata')}
        attributeList={credentialDisplayData.metadata}
      />
    </View>
  );
};

export default CardWithDetails;
