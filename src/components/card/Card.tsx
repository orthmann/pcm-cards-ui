import { useEffect, useState } from 'react';
import { Image, ImageBackground, Text, View } from 'react-native';
import { fontFamily } from '../../styles/styles';
import { useStyles } from '../../styles/useStyles';
import { CardDisplay, CardRenderEntry, CredentialDisplayData } from '../../types/types';
import CardStatus from './CardStatus';

interface CardProps {
  credentialData: CredentialDisplayData;
  stacked?: boolean;
}

const DEFAULT_WIDTH = 800;
const MAX_ATTRIBUTES = 4;

function createBackground(cardDisplay: CardDisplay) {
  return {
    type: 'bg',
    data: [cardDisplay.backgroundImage || ''],
    dimension: { x: 0, y: 0, w: DEFAULT_WIDTH, h: 450 },
    color: cardDisplay.backgroundColor || '#cccccc',
  };
}

function createLogo(cardDisplay: CardDisplay) {
  if (cardDisplay.logoURL) {
    return {
      type: 'image',
      data: [cardDisplay.logoURL],
      dimension: { x: 25, y: 25, w: 41, h: 41 },
    };
  }
  return null;
}

function createTitle(cardDisplay: CardDisplay) {
  if (cardDisplay.title) {
    return {
      type: 'text',
      data: [`$.display.name.value`],
      dimension: { x: 130, y: 25, h: 41 },
    };
  }
  return null;
}

function createAttributeText(key: string, index: number) {
  const x = index < MAX_ATTRIBUTES ? 25 : DEFAULT_WIDTH / 2;
  const y = index < MAX_ATTRIBUTES ? 120 + index * 80 : 120 + (index - MAX_ATTRIBUTES) * 80;

  return [
    {
      type: 'text',
      data: [`$.credentialSubject.${key}.label`, ':'],
      dimension: { x, y, h: 24 },
    },
    {
      type: 'text',
      data: [`$.credentialSubject.${key}.value`],
      dimension: { x, y: y + 30, h: 33, w: DEFAULT_WIDTH / 2 - x },
    },
  ];
}

const Card = ({ credentialData, stacked = false }: CardProps) => {
  const styles = useStyles();

  const [initialized, setInitialized] = useState<boolean>(false);
  const [scaleX, setScaleX] = useState<number>(1);
  const [scaleY, setScaleY] = useState<number>(1);
  const [canvasW, setCanvasW] = useState<number>(800);
  const [canvasH, setCanvasH] = useState<number>(505);

  const { cardDisplay, attributes, renderHint } = credentialData;

  let render: CardRenderEntry[] = renderHint ? [...renderHint] : [];

  if (!render || render.length === 0) {
    render = [
      createBackground(cardDisplay),
      createLogo(cardDisplay),
      createTitle(cardDisplay),
      ...(attributes
        ? Object.keys(attributes)
            .slice(0, MAX_ATTRIBUTES * 2)
            .flatMap((key, index) => createAttributeText(key, index))
        : []),
    ].filter(Boolean) as CardRenderEntry[];
  }

  useEffect(() => {
    getCanvasDimension(render);
  }, []);

  function getCanvasDimension(render: CardRenderEntry[]) {
    for (const entry of render) {
      if (entry.type === 'bg') {
        setCanvasH(entry.dimension.h!);
        setCanvasW(entry.dimension.w!);
      }
    }
    setInitialized(true);
  }

  const onLayout = (event: any) => {
    const { height, width } = event.nativeEvent.layout;

    const scaleX = width / canvasW;
    const scaleY = height / canvasH;
    setScaleX(scaleX);
    setScaleY(scaleY);
  };

  function renderBGImage(p: CardRenderEntry, index: number) {
    if (!p.data || p.data[0] === '') {
      return (
        <View
          key={index}
          style={{
            backgroundColor: p.color,
            height: '100%',
            width: '100%',
          }}
        />
      );
    }

    return (
      <ImageBackground
        key={index}
        style={{
          backgroundColor: p.color,
          height: '100%',
          width: '100%',
          top: 0,
        }}
        source={{ uri: p.data[0]! }}
      />
    );
  }

  function renderImage(p: CardRenderEntry, index: number) {
    return (
      <Image
        key={index}
        style={{
          position: 'absolute',
          top: p.dimension.y! * scaleY,
          left: p.dimension.x! * scaleX,
          height: p.dimension.h,
          width: p.dimension.w,
        }}
        source={{ uri: p.data[0] }}
      />
    );
  }

  function renderText(p: CardRenderEntry, index: number) {
    return (
      <View
        key={index}
        style={{
          display: 'flex',
          flexDirection: 'row',
          position: 'absolute',
          top: p.dimension.y! * scaleY,
          left: p.dimension.x! * scaleX,
          maxWidth: p.dimension.w ? scaleX * (p.dimension.w - 20) : scaleX * (canvasW - p.dimension.x! - 20),
        }}
      >
        {p.data && p.data.map && p.data.map((entry, dindex) => renderTextEntry(entry, p, dindex))}
      </View>
    );
  }

  function renderTextEntry(path: string, p: CardRenderEntry, key: number) {
    const pval = pathToString(path);

    let fontWeight = '400';
    if (p.weight) {
      fontWeight = p.weight;
    }
    let fontSize = 15;
    if (p.dimension.h) {
      fontSize = p.dimension.h;
    }

    return (
      <Text
        key={key}
        style={{
          ...fontFamily.teleNeoOffice,
          fontSize: fontSize * scaleY,
          textAlignVertical: 'top',
          color: credentialData.cardDisplay.textColor || 'black',
          opacity: pval.type && pval.type === 'label' ? 1.0 : pval.disclosed ? 1.0 : 0.3,
          // @ts-ignore: 2769
          fontWeight,
        }}
        numberOfLines={1}
      >
        {pval.text}
      </Text>
    );
  }

  function pathToString(path: string): { text: string; disclosed: boolean; type?: string } {
    const attributes = credentialData.attributes;

    if (path.startsWith('$.')) {
      if (path === '$.display.name.value') {
        return { text: credentialData.cardDisplay.title || '', disclosed: true };
      }

      path = path.replace('$.credentialSubject.', '');
      const [pval, ptype] = path.split('.');

      if (ptype === undefined || pval === undefined) {
        return { text: '!2!' + path, disclosed: true };
      }

      if (!attributes[pval] || !attributes[pval].label) {
        return { text: '?' + path, disclosed: true };
      }

      if (ptype === 'label') {
        return {
          text: attributes[pval].label || '',
          disclosed: true,
          type: 'label',
        };
      } else {
        return { text: attributes[pval].value, disclosed: attributes[pval].disclosed, type: 'text' };
      }
    } else {
      return { text: path, disclosed: true };
    }
  }

  function floatingDataset(p: CardRenderEntry, index: number) {
    if (p.type === 'bg') {
      return renderBGImage(p, index);
    }

    if (!initialized) {
      return <Text key={index}>loading...</Text>;
    }

    if (p.type === 'image') {
      return renderImage(p, index);
    } else if (p.type === 'text') {
      return renderText(p, index);
    }

    return <></>;
  }

  function renderView() {
    try {
      const result = (
        <View
          onLayout={onLayout}
          style={[
            styles.card,
            styles.cardBorderColor,
            {
              aspectRatio: canvasW / canvasH,
            },
          ]}
        >
          {render.map((entry, index) => floatingDataset(entry, index))}
        </View>
      );

      return result;
    } catch (e) {
      return (
        <View style={{ height: '100%', width: '100%', aspectRatio: 800 / 500 }}>
          <Text>Failed to render CardLayout.</Text>
        </View>
      );
    }
  }

  return (
    <>
      <View style={[stacked ? styles.cardStacked : {}]}>{renderView()}</View>
      <CardStatus status={credentialData.cardDisplay} />
    </>
  );
};

export default Card;
