import React from 'react';
import { Pressable, Text } from 'react-native';
import { useStyles } from '../../styles/useStyles';

interface ButtonProps {
  text: string;
  active: boolean;
  variant: 'primary' | 'secondary';
  callback: any;
}

const Button = ({ text, active, variant, callback }: ButtonProps) => {
  const styles = useStyles();

  return (
    <Pressable
      style={styles.button}
      onPress={() => {
        if (active) callback();
      }}
    >
      <Text
        style={[
          styles.button,
          variant === 'primary'
            ? active
              ? styles.buttonPrimary
              : styles.buttonPrimaryInactive
            : styles.buttonSecondary,
        ]}
      >
        {text}
      </Text>
    </Pressable>
  );
};

export default Button;
