import { loadAsync } from 'expo-font';
import { useEffect, useState } from 'react';

const FontLoader = ({ children }: { children: JSX.Element }) => {
  const [fontLoaded, setFontLoaded] = useState(false);

  useEffect(() => {
    loadAsync({
      TeleNeoOffice: require('../../../assets/fonts/TeleNeoOffice.ttf'),
      TeleNeoOfficeBold: require('../../../assets/fonts/TeleNeoOffice_bold.ttf'),
      TeleNeoOfficeMedium: require('../../../assets/fonts/TeleNeoOffice_medium.ttf'),
      TeleNeoOfficeLight: require('../../../assets/fonts/TeleNeoOffice_thin.ttf'),
      TeleNeoOfficeUltra: require('../../../assets/fonts/TeleNeoOffice_ultra.ttf'),
      TeleNeoOddiceExtraBold: require('../../../assets/fonts/TeleNeoOffice_extra_bold.ttf'),
      TeleNeoOfficeItalic: require('../../../assets/fonts/TeleNeoOffice_italic.ttf'),
      TeleNeoOfficeBoldItalic: require('../../../assets/fonts/TeleNeoOffice_bold_italic.ttf'),
      TeleNeoOfficeMediumItalic: require('../../../assets/fonts/TeleNeoOffice_medium_italic.ttf'),
      TeleNeoOfficeLightItalic: require('../../../assets/fonts/TeleNeoOffice_thin_italic.ttf'),
      TeleNeoOfficeUltraItalic: require('../../../assets/fonts/TeleNeoOffice_ultra_italic.ttf'),
      TeleNeoOfficeExtraBoldItalic: require('../../../assets/fonts/TeleNeoOffice_extra_bold_italic.ttf'),
    }).then(() => setFontLoaded(true));
  }, []);

  if (!fontLoaded) return null;

  return children;
};

export default FontLoader;
