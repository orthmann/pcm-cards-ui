export interface Display {
  name: string;
  locale: string;
}

export interface Image {
  url: string;
  alternative_text: string;
}

export interface CredentialViewItem {
  credential: {
    key_id: string;
    raw_data: {
      format: string;
      credential: string;
    };
    presentation: {
      credentialSubject: {
        [key: string]: string;
      };
      _sd_alg: string;
      cnf: {
        jwk: {
          alg: string;
          crv: string;
          kty: string;
          x: string;
          y: string;
        };
      };
      type: string;
      iss: string;
      exp: number;
      nbf: number;
      iat: number;
    };
  };
  well_known: {
    id: string;
    format: string;
    type: string;
    cryptographic_binding_methods_supported: string[];
    cryptographic_suites_supported: string[];
    render_hint?: CardRenderEntry[];
    display: {
      name: string;
      locale: string;
      logo: Image;
      background_image?: Image;
      background_color: string;
      text_color: string;
    }[];
    credentialSubject: {
      [key: string]: {
        display: Display[];
      };
    };
  };
}

export type CredentialView = Map<string, CredentialViewItem>;

export interface CredentialDisplayData {
  cardDisplay: CardDisplay;
  attributes: CredentialDisplayAttributeList;
  metadata: CredentialDisplayAttributeList;
  renderHint: CardRenderEntry[];
}

export interface CardDisplay {
  title?: string;
  logoURL?: string;
  textColor?: string;
  backgroundImage?: string;
  backgroundColor?: string;
  verified?: boolean;
  cloud?: boolean;
}

export type CredentialDisplayAttributeList = Record<string, CredentialDisplayAttribute>;

export interface CredentialDisplayAttribute {
  label?: string;
  value: string;
  disclosed: boolean;
  i18n?: string;
}

export interface CardRenderEntry {
  type: string;
  data: string[];
  dimension: Dimension;
  color?: string;
  weight?: string;
}

export interface Dimension {
  x?: number;
  y?: number;
  w?: number;
  h?: number;
}
