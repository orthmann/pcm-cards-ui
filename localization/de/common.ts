// SPDX-FileCopyrightText:  2023 DTIT and TLABS.
// SPDX-License-Identifier: LicenseRef-INTERNAL-USE-ONLY
export default {
  username: 'Nutzername',
  password: 'Passwort',
  login: 'Login',
  welcome: 'Willkommen',
  language: 'Sprache',
  change_language: 'Sprache ändern',
  confirm: 'Bestätigen',
  show: 'Anzeigen',
  email: 'E-Mail',
  next: 'Weiter',
  skip: 'Überspringen',
  lets_go: "Los geht's",
  accept: 'Akzeptieren',
  reject: 'Ablehnen',
  local: 'Lokal',
  cancel: 'Abbrechen',
};
