// SPDX-FileCopyrightText:  2023 DTIT and TLABS.
// SPDX-License-Identifier: LicenseRef-INTERNAL-USE-ONLY
import common from './common';
import navigation from './navigation';
import onboarding from './onboarding';
import wallet from './wallet';

export default {
  common,
  navigation,
  onboarding,
  wallet,
};
