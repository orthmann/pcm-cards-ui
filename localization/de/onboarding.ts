// SPDX-FileCopyrightText:  2023 DTIT and TLABS.
// SPDX-License-Identifier: LicenseRef-INTERNAL-USE-ONLY
export default {
  bar_general: 'Allgemein',
  bar_email: 'Email',
  bar_security: 'App Sicherheit',
  bar_eid: 'eID',
  welcome_label_first: 'Willkommen bei der T-Wallet',
  welcome_text_first:
    'Ihre elektronische Brieftasche - Speichern Sie Ihren Ausweis, um Ihre Identität jederzeit nachzuweisen. Online und Offline.',
  welcome_label_second: 'Fügen sie einfach weitere Dokumente hinzu',
  welcome_text_second:
    'Ergänzen Sie Ihre T-Wallet um weitere Dokumente - zum Beispiel Ihren Führerschein oder Mitgliedsauweise.',
  welcome_label_third: 'Höchste Sicherheitsstufe',
  welcome_text_third:
    'Die Berechtigungen und Ausweise, die Sie in Ihrer T-Wallet hinterlegen, werden sicher verschlüsselt. Sie alleine entscheiden, wer darauf Zugriff erhält.',
  next_to_installation: 'Weiter zur Installation',
  terms_and_conditions: 'Ich akzeptiere die allgemeinen Geschäftsbedingungen',
  protection_information: 'Ich akzeptiere die Datenschutzhinweise',
  email_text: 'Bitte geben Sie zur Verifikation Ihrer T-Wallet ihre E-Mail-Adresse ein',
  repeat_email: 'E-Mail wiederholen',
  set_new_pin: 'Neue T-Wallet PIN vergeben',
  confirm_pin: 'Bitte T-Wallet PIN bestätigen',
  add_id_card: 'Fügen Sie Ihren Personalausweis Ihrem T-Wallet hinzu',
  create_eu_id: ' Möchten Sie sich auch eine digitale EU Identität erstellen lassen?',
  eu_rights:
    'Jede Person mit Anspruch auf einen nationalen Personalausweis hat das Recht, eine digitale Identität zu besitzen, die EU-weit anerkannt wird.',
  eu_information: 'Weitere Informationen zur digitalen EU Identität',
  eu_create_confirmation: 'Ja, ich möchte gleichzeitig die digitale EU Identität erhalten',
  scan_id: 'Direktes Auslesen des Personalausweises',
  hold_id_to_phone:
    'Platzieren sie Ihren Ausweis direkt an der Rückseite des Smartphones, üblicherweise im oberen Bereich.',
  activate_nfc: 'Bitte aktivieren Sie den NFC Reader.',
  wallet_ready: 'Ihre T-Wallet App ist jetzt startklar',
  wallet_ready_for_use: 'Herzlichen Glückwunsch, Sie könnten die App jetzt nutzen.',
};
