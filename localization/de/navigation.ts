// SPDX-FileCopyrightText:  2023 DTIT and TLABS.
// SPDX-License-Identifier: LicenseRef-INTERNAL-USE-ONLY
export default {
  app_name: 'T-Wallet',
  settings: 'Einstellungen',
  general: 'Allgemein',
  info: 'Info',
  imprint: 'Impressum',
  account: 'Konto',
  email: 'Email',
  developer_mode: 'Entwickler Modus',
  cloud_storage: 'Cloud Spiecher',
  restart_onboarding: 'Onboarding neustarten',
  restart_onboarding_eid: 'Onboarding @ eID',
  reset_onboarded: 'Onboarding zurücksetzen',
  add_stbk: 'Steuerberraterkammer Nachweis hinzufügen',
  add_tmember: 'Telekom Mitarbeiter Nachweis hinzufügen',
  add_tlabs: 'T-Labs Raumbuchungs Nachweis hinzufügen',
  add_barmer: 'Gesundheitskarten Nachweis hinzufügen',
  details: 'Detailansicht',
};
