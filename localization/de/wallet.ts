// SPDX-FileCopyrightText:  2023 DTIT and TLABS.
// SPDX-License-Identifier: LicenseRef-INTERNAL-USE-ONLY
export default {
  empty_wallet: 'Ihre T-Wallet ist leer',
  empty_wallet_info:
    'Speichern Sie jetzt Ihr Ausweisdokument in Ihrer Wallet App. Befolgen Sie dazu die nachfolgenden Anweisungen',
  empty_wallet_add: 'Ausweisdokument hinzufügen',
  credential_details: 'Nachweisdetails',
  metadata: 'Metadaten',
  delete_credential: 'Nachweis entfernen',
  delete_credential_confirmation: 'Nachweis Entfernen bestätigen?',
  login: 'Zum einloggen Wallet-Pin eingeben',
  issuer: 'Herausgeber',
  issue_date: 'Herausgabedatum',
  valid_from: 'Gültig ab',
  expiry_date: 'Gültig bis',
  type: 'Typ',
  requested_fields: 'Angefragte Felder',
  no_creds_found: 'Keine passenden Nachweise gefundens',
  scan_again: 'Nochmal Scannen',
  id_card_text: 'Sie können nun Ihren Personalausweis in Ihrer T-Wallet speichern',
  eu_id_card_text: 'Sie können nun Ihre Europäische Digitale Identität in Ihrer T-Wallet speichern',
  card_check: 'Karte wird geprüft',
  authenticating: 'Authentisierung wird durchgeführt',
};
