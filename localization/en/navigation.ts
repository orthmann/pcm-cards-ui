// SPDX-FileCopyrightText:  2023 DTIT and TLABS.
// SPDX-License-Identifier: LicenseRef-INTERNAL-USE-ONLY
export default {
  app_name: 'T-Wallet',
  settings: 'Settings',
  general: 'General',
  info: 'Info',
  imprint: 'Imprint',
  account: 'Account',
  email: 'Email',
  developer_mode: 'Devloper Mode',
  cloud_storage: 'Cloud Storage',
  restart_onboarding: 'Restart Onboarding',
  restart_onboarding_eid: 'Onboarding @ eID',
  reset_onboarded: 'Reset Onboarded',
  add_stbk: 'Add Chamber of Tax advisors Credential',
  add_tmember: 'Add Telekom Employee Credential',
  add_tlabs: 'Add T-Labs booking Credential',
  add_barmer: 'Add Health Card Credential',
  details: 'Details',
};
