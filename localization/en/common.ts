// SPDX-FileCopyrightText:  2023 DTIT and TLABS.
// SPDX-License-Identifier: LicenseRef-INTERNAL-USE-ONLY
export default {
  username: 'Username',
  password: 'Password',
  login: 'Login',
  welcome: 'Welcome',
  language: 'Language',
  change_language: 'Change Language',
  confirm: 'Confirm',
  show: 'Show',
  email: 'Email',
  next: 'Next',
  skip: 'Skip',
  lets_go: "Let's go",
  accept: 'Accept',
  reject: 'Reject',
  local: 'Local',
  cancel: 'cancel',
};
