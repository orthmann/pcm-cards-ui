// SPDX-FileCopyrightText:  2023 DTIT and TLABS.
// SPDX-License-Identifier: LicenseRef-INTERNAL-USE-ONLY
export default {
  bar_general: 'General',
  bar_email: 'Email',
  bar_security: 'App Security',
  bar_eid: 'eID',
  welcome_label_first: 'Welcome to the T-Wallet',
  welcome_text_first:
    'Your digital Wallet - Secure your ID to prove your identity at all times. Online and Offline.',
  welcome_label_second: 'Simply add additional documents',
  welcome_text_second:
    "Add additional documents to your T-Wallet - for example your driver's license or membership cards.",
  welcome_label_third: 'Highest security level',
  welcome_text_third:
    'The authorizations and ID cards you store in your T-Wallet are securely encrypted. You alone decide who has access to it.',
  next_to_installation: 'Proceed to installation',
  terms_and_conditions: 'I accept the terms of service',
  protection_information: 'I accept the data protection information',
  email_text: 'To verify your T-Wallet, please enter your email address',
  repeat_email: 'Repeat Email',
  set_new_pin: 'Assign new T-Wallet PIN',
  confirm_pin: 'Please confirm T-Wallet PIN',
  add_id_card: 'Add your ID card to your T-Wallet',
  create_eu_id: 'Would you also like to have a digital EU identity created?',
  eu_rights:
    'Every person entitled to a national identity card has the right to one have a digital identity that is recognized across the EU.',
  eu_information: 'Further information on digital EU identity',
  eu_create_confirmation: 'Yes, I would like to receive the digital EU identity at the same time',
  scan_id: 'Direct reading of the ID card',
  hold_id_to_phone:
    'Place your ID card directly on the back of the smartphone, usually in the upper area.',
  activate_nfc: 'Please activate the NFC reader.',
  wallet_ready: 'Your T-Wallet app is now ready to go',
  wallet_ready_for_use: 'Congratulations, you could use the app now.',
};
