// SPDX-FileCopyrightText:  2023 DTIT and TLABS.
// SPDX-License-Identifier: LicenseRef-INTERNAL-USE-ONLY
export default {
  empty_wallet: 'Your T-Wallet is empty',
  empty_wallet_info:
    'Now save your ID document in your Wallet app. To do this, follow the instructions below',
  empty_wallet_add: 'Add ID document',
  credential_details: 'Credential Details',
  metadata: 'Metadata',
  delete_credential: 'Delete Credential',
  delete_credential_confirmation: 'Delete Credential confirmation ?',
  login: 'Enter Wallet-Pin to login',
  issuer: 'Issuer',
  issue_date: 'Issue Date',
  valid_from: 'Valid from',
  expiry_date: 'Expiry Date',
  type: 'Type',
  requested_fields: 'Requested Fields',
  no_creds_found: 'No matching credentials found',
  scan_again: 'Scan again',
  id_card_text: 'You can now save your ID card to your T-Wallet',
  eu_id_card_text: 'You can now store your European Digital Identity in your T-Wallet',
  card_check: 'Card is checked',
  authenticating: 'Authentication is being carried out',
};
