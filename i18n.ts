import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import de from './localization/de';
import en from './localization/en';

const resources = {
  en,
  de,
};

i18n.use(initReactI18next).init({
  compatibilityJSON: 'v3',
  lng: 'en',
  fallbackLng: 'en',
  resources,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
