import FontLoader from './src/components/font-loader/FontLoader';
import './i18n';
import Main from './src/components/views/Main';

export default function App() {
  return (
    <FontLoader>
      <Main />
    </FontLoader>
  );
}
